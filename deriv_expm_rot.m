function [dR_dw1, dR_dw2, dR_dw3] = deriv_expm_rot(w)
R = expm(skew(w));

dR_dw1 = (w(1)*skew(w) + skew(cross(w,(eye(3) - R)*[1;0;0])))/norm(w)^2*R;
dR_dw2 = (w(2)*skew(w) + skew(cross(w,(eye(3) - R)*[0;1;0])))/norm(w)^2*R;
dR_dw3 = (w(3)*skew(w) + skew(cross(w,(eye(3) - R)*[0;0;1])))/norm(w)^2*R;
end