clear;clc;close all;
numsteps = 100;
w_traj2w = [0.5;0;0.];
true_Rot_traj2w = expm(skew(pi*0.2.*w_traj2w./norm(w_traj2w)));
true_trans_traj2w = [5;5;5];

true_r = [inv_skew(logm(true_Rot_traj2w')); true_Rot_traj2w'*[-3;0;0]];
true_x = [0;0;0];
[z,visible_flag] = observe(true_r, true_x);
true_x_cum = visualize(true_r, true_x, [], true_r, true_x, z, visible_flag, true_Rot_traj2w, true_trans_traj2w);
Codebook = code_define_ellipses(numsteps);

for ii = 1:10
    [true_r, true_x] = state_trans(true_r,true_x,[0;0;0;10*pi/180],Codebook,ii,1);
    [z,visible_flag] = observe(true_r, true_x);
    true_x_cum = visualize(true_r, true_x, true_x_cum, true_r, true_x, z, visible_flag, true_Rot_traj2w, true_trans_traj2w);
    pause(0.1);
end