function Gr = G_dr_num(r,x)
    function z = observe(r,x)
        P = proj_mat(r);
        proj = P*[x;1];
        z = [proj(1); proj(2)]./proj(3);
    end
del = 1.e-8;
Gr = zeros(2,6);
h_base = observe(r,x);
r_purt = r;
for ii = 1:length(r)
    r_purt(ii) = r_purt(ii) + del;
    h_purt = observe(r_purt,x);
    Gr(:,ii) = (h_purt - h_base)/del;
    r_purt(ii) = r(ii);
end
end