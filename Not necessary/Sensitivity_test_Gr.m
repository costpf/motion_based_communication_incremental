clear;clc;
maximum_so_far = 0;
for ii = 1:5000
    fprintf('Trial: %d\n', ii)
    r = randn(6,1);
    x = randn(3,1);
    z = observe(r,x);
    Gr = G_dr(r,x);
    S = ((1./z)*r').*Gr;
    s = reshape(S,1,12);
    if maximum_so_far < max(abs(s))
        maximum_so_far = max(abs(s));
        disp(S)
    end
end
disp(maximum_so_far);