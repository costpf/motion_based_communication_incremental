function Fr = F_dr_num(r,u)
    function r_new = state(r,u)
        w = [r(1);r(2);r(3)];
        t = [r(4);r(5);r(6)];
        w_new = inv_skew(logm(expm(skew(w))*expm(skew([0;0;u(4)]))));
        t_new = t + expm(skew(w))*[u(1);u(2);u(3)];
        r_new = [w_new;t_new];
    end
del = 1.e-8;
Fr = zeros(6,6);
f_base = state(r,u);
r_purt = r;
for ii = 1:length(r)
    r_purt(ii) = r_purt(ii) + del;
    f_purt = state(r_purt,u);
    Fr(:,ii) = (f_purt - f_base)/del;
    r_purt(ii) = r(ii);
end
end