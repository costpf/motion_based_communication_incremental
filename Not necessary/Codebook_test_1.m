numsteps = 10;
Codebook = code_define_ellipses(numsteps);
true_x_cum = cell(3,1);
for ii = 1:length(Codebook)
    true_x_cum{ii} = zeros(3,1);
end
figure(1);

for tt = 2:numsteps
    clf(figure(1));
    grid on; hold on;
    for ii = 1:length(Codebook)
        true_x_cum{ii}(:,end+1) = true_x_cum{ii}(:,end) + Codebook{ii}(:,tt-1);
        scatter3(true_x_cum{ii}(1,:), true_x_cum{ii}(2,:), true_x_cum{ii}(3,:));
    end
    axis equal;
    pause(0.01);
end

