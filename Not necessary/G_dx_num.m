function Gx = G_dx_num(r,x)
    function z = observe(r,x)
        P = proj_mat(r);
        proj = P*[x;1];
        z = [proj(1); proj(2)]./proj(3);
    end
del = 1.e-8;
Gx = zeros(2,3);
h_base = observe(r,x);
x_purt = x;
for ii = 1:length(x)
    x_purt(ii) = x_purt(ii) + del;
    h_purt = observe(r,x_purt);
    Gx(:,ii) = (h_purt - h_base)/del;
    x_purt(ii) = x(ii);
end
end