w = rand(3,1);
u = [0;0;rand()];
A = expm(skew(u))*expm(skew(w))
S = logm(A);
[deS1,deS2,deS3] = deriv_expm_rot(w);

B = [A, expm(skew(u))*deS3;
     zeros(3,3), A];
dS_a = logm(B); % Frechet Derivative. See: http://math.stackexchange.com/questions/1639050/derivative-of-matrix-logarithm-with-respect-to-matrix
dS_a = dS_a(1:3,4:6)
del = 1.e-8;
S_purt = logm(expm(skew(u))*expm(skew(w + [0;0;del])));
dS_n = (S_purt - S)./del