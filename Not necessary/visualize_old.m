function true_x_cum = visualize(true_r, true_x, true_x_cum, est_r, est_x, z, visible_flag, Rot_traj2w)
    function [true_camera_pose, est_camera_pose, true_r, true_x, est_r, est_x, true_x_cum] = convert2world(true_r, true_x, est_r, est_x, true_x_cum, Rot_traj2w, R_bc, d_bc)
        % In the trajectory coordinates.
        true_t = true_r(4:6);
        true_w = true_r(1:3);
        est_t = est_r(4:6);
        est_w = est_r(1:3);
        
        % Convert to world coordinates.
        true_t = Rot_traj2w*true_t;
        true_w = inv_skew(logm(Rot_traj2w*expm(skew(true_w))));
        if norm(true_w) > pi; % Avoid singularities.
            true_w = (1 - 2*pi/norm(true_w))*true_w;
        end
        true_r = [true_w; true_t];
        
        est_t = Rot_traj2w*est_t;
        est_w = inv_skew(logm(Rot_traj2w*expm(skew(est_w))));
        if norm(est_w) > pi; % Avoid singularities.
            est_w = (1 - 2*pi/norm(est_w))*est_w;
        end
        est_r = [est_w; est_t];
        
        true_x = Rot_traj2w*true_x;
        est_x = Rot_traj2w*est_x;
        true_x_cum(:,end+1) = true_x;
        
        % calculate camera pose in the world coordinates.(external parameters matrix with [R|t])
        true_camera_rot = expm(skew(true_w))*R_bc';
        true_camera_trans = true_t + expm(skew(true_w))*[d_bc;0;0];
        true_camera_pose = [true_camera_rot, true_camera_trans];
        
        est_camera_rot =  expm(skew(est_w))*R_bc';
        est_camera_trans = est_t + expm(skew(est_w))*[d_bc;0;0];
        est_camera_pose = [est_camera_rot, est_camera_trans];
    end

load('camera_param.mat', 'K', 'R_bc', 'd_bc');
u_max = 2*K(1,3);
v_max = 2*K(2,3);

% values are in the world coordinates.
[true_camera_pose, est_camera_pose, true_r, ~, est_r, est_x, true_x_cum] = convert2world(true_r, true_x, est_r, est_x, true_x_cum, Rot_traj2w, R_bc, d_bc);

clf(figure(1));
figure(1);
set(gcf, 'Position', [1050, 400, 0.5*u_max, 0.5*v_max]);
hold on;
set(gca,'Ydir','reverse');
if visible_flag == 1
    scatter(z(1), z(2), 'ro', 'linewidth',2.0);
end
xlabel('u');
ylabel('v');
plot([0,u_max,u_max,0,0],[0,0,v_max,v_max,0],'m');
axis equal;
drawnow;

clf(figure(2));
figure(2);
set(gcf, 'Position', [100, 100, 900, 700]);
grid on; hold on;
% draw world axes.
line([0,4],[0,0],[0,0],'Linestyle','--','Color','r');
line([0,0],[0,4],[0,0],'Linestyle','--','Color','g');
line([0,0],[0,0],[0,4],'Linestyle','--','Color','b');
xlabel('X_w'); ylabel('Y_w'); zlabel('Z_w');
% draw trajectory axes.
e1_traj = Rot_traj2w*[4;0;0];
e2_traj = Rot_traj2w*[0;4;0];
e3_traj = Rot_traj2w*[0;0;4];
line([0,e1_traj(1)],[0,e1_traj(2)],[0,e1_traj(3)],'Linestyle','-','Color','r');
line([0,e2_traj(1)],[0,e2_traj(2)],[0,e2_traj(3)],'Linestyle','-','Color','g');
line([0,e3_traj(1)],[0,e3_traj(2)],[0,e3_traj(3)],'Linestyle','-','Color','b');

% draw camera axes.
e1_cam = true_camera_pose(:,1:3)*[2;0;0] + true_camera_pose(:,4);
e2_cam = true_camera_pose(:,1:3)*[0;2;0] + true_camera_pose(:,4);
e3_cam = true_camera_pose(:,1:3)*[0;0;2] + true_camera_pose(:,4);
line([true_camera_pose(1,4),e1_cam(1)],[true_camera_pose(2,4),e1_cam(2)],[true_camera_pose(3,4),e1_cam(3)],'Linestyle','-.','Color','r');
line([true_camera_pose(1,4),e2_cam(1)],[true_camera_pose(2,4),e2_cam(2)],[true_camera_pose(3,4),e2_cam(3)],'Linestyle','-.','Color','g');
line([true_camera_pose(1,4),e3_cam(1)],[true_camera_pose(2,4),e3_cam(2)],[true_camera_pose(3,4),e3_cam(3)],'Linestyle','-.','Color','b');
axis equal;


if size(true_x_cum,2) > 1
    scatter3(true_x_cum(1,1:end-1),true_x_cum(2,1:end-1),true_x_cum(3,1:end-1),'y');
end
scatter3(true_r(4),true_r(5),true_r(6),'r');
scatter3(est_r(4),est_r(5),est_r(6),'b');
scatter3(true_x_cum(1,end),true_x_cum(2,end),true_x_cum(3,end),'r');
scatter3(est_x(1),est_x(2),est_x(3),'b');
plotCamera('Location',true_camera_pose(:,4)','Orientation',transpose(true_camera_pose(:,1:3)),'Opacity',0, 'Size',0.4); 
plotCamera('Location',est_camera_pose(:,4)','Orientation',transpose(est_camera_pose(:,1:3)),'Opacity',0, 'Size',0.4,'Color','b'); 
axis equal;
%xlim([-15,15]);
%ylim([-15,15]);
%zlim([-15,15]);
if isequal(get(gca,'View'), [0 90]);
    view(0,90); % change default view angle.
end
drawnow;
end