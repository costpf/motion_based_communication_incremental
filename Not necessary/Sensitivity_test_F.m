clear;clc;
maximum_so_far = 0;
for ii = 1:5000
    fprintf('Trial: %d\n', ii)
    r = randn(6,1);
    u = randn(4,1);
    F = F_dr(r,u);
    
    w = r(1:3);
    t = r(4:6);
    t = t + expm(skew(w))*[u(1);u(2);u(3)];
    w = inv_skew(logm(expm(skew(w))*expm(skew([0;0;u(4)]))));
    
    r_new = [w;t];
    
    
    S = ((1./r_new)*r').*F;
    s = reshape(S,1,36);
    if maximum_so_far < max(abs(s))
        maximum_so_far = max(abs(s));
    end
end
disp(maximum_so_far);