function [J1, J2] = J_state(log_phi, mu, Sigma, u, Codebook, timestep)
load('noise_param.mat');load('camera_param.mat');
phi = retrieve(log_phi);

H = cell(length(Codebook),1);
Sig = cell(length(Codebook),1);
Sig_bar = cell(length(Codebook),1);
logdet_2piH = zeros(length(Codebook),1);
logdet_2piSig = zeros(length(Codebook),1);
z_est = cell(length(Codebook),1);
alpha = zeros(length(Codebook),1);
opt_mean = zeros(2,1);
state_mean = zeros(9,1);


% Check whether H(m) is small enough.
thresh = 1.e-310;
if -dot(phi,log_phi) < thresh
    [~,index_phi_max] = max(phi);
    
    [r_bar_max,x_bar_max,~,~,~,H_max,~,Sig_max] = belief_propagate(mu{index_phi_max}(1:6),mu{index_phi_max}(7:9),Sigma{index_phi_max},u,Codebook,timestep,index_phi_max);
    J1 = 1/2*logdet(2*pi*Sig_max);
    
    U0 = K(1,3);
    V0 = K(2,3);
    opt_mean_max = observe(r_bar_max, x_bar_max);
    %J2 = norm(opt_mean_max)^2 + trace(H_max) - 2*dot([U0,V0],opt_mean_max) + dot([U0,V0], [U0,V0]);
    %J2 = norm(opt_mean_max)^2 + trace(H_max) - 2*dot([U0,V0],opt_mean_max) + dot([U0,V0], [U0,V0]);
    J2 = norm(opt_mean_max - [U0;V0])^2;
else
    for ii = 1:length(Codebook)
        r = mu{ii}(1:6);
        x = mu{ii}(7:9);
        [r_bar,x_bar,Sig_bar{ii},~,~,H{ii},~,Sig{ii}] = belief_propagate(r,x,Sigma{ii},u,Codebook,timestep,ii);
        logdet_2piH(ii) = logdet(2*pi*H{ii});
        logdet_2piSig(ii) = logdet(2*pi*Sig{ii});
        z_est{ii} = observe(r_bar,x_bar);
        
        opt_mean = opt_mean + phi(ii)*z_est{ii};
        state_mean = state_mean + phi(ii)*[r_bar;x_bar];
    end
    
% %     % Compute (1st-order-approximated) gaussian mixture entropy
%     for ii = 1:length(Codebook)
%         exponents = zeros(length(Codebook),1);
%         for jj = 1:length(Codebook)
%             exponents(jj) = -1/2*(z_est{ii} - z_est{jj})'*(H{jj}\(z_est{ii} - z_est{jj}));
%         end
%         alpha(ii) = logsumexp(log_phi,exponents);
%     end
    % Compute (2nd-order-approximated) gaussian mixture entropy
    Entropy_gm = entropy_mvgm_2(phi, z_est, H);
    
    % Compute the Joint-belief-state entropy
    J1 = dot(phi, (1/2.*logdet_2piH)) + dot(phi, (1/2.*logdet_2piSig)) - Entropy_gm;%+ dot(phi, alpha);
    
    
    % Optical cost function.
    U0 = K(1,3);
    V0 = K(2,3);
    
    opt_var = zeros(2,2);
    for ii = 1:length(Codebook)
        opt_var = opt_var + phi(ii)*H{ii} + phi(ii)*(z_est{ii} - opt_mean)*(z_est{ii} - opt_mean)';
    end
    
    % J2 = norm(opt_mean)^2 + trace(opt_var) - 2*dot([U0,V0],opt_mean) + dot([U0,V0], [U0,V0]);
    J2 = norm(opt_mean - [U0;V0])^2;
end

if isinf(J1);
    disp('J is -Inf!!');
end
if isnan(J1);
    disp('J is NaN!!');
end