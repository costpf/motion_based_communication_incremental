function Fr = F_dr(r,u)
Fr = zeros(6,6);
w = [r(1);r(2);r(3)];
[R1,R2,R3] = deriv_expm_rot(w);

u_t = [u(1);u(2);u(3)];
Fr(4:6,1:3) = [R1*u_t, R2*u_t, R3*u_t];

A = expm(skew(w))*expm(skew([0;0;u(4)]));
% Frechet Derivative. See: http://math.stackexchange.com/questions/1639050/derivative-of-matrix-logarithm-with-respect-to-matrix
B1 = [A, R1*expm(skew([0;0;u(4)]));
      zeros(3), A];
B2 = [A, R2*expm(skew([0;0;u(4)]));
      zeros(3), A];
B3 = [A, R3*expm(skew([0;0;u(4)]));
      zeros(3), A];
C1 = logm(B1);
C1 = C1(1:3,4:6);
C2 = logm(B2);
C2 = C2(1:3,4:6);
C3 = logm(B3);
C3 = C3(1:3,4:6);
Fr(1:3,1) = inv_skew(C1);
Fr(1:3,2) = inv_skew(C2);
Fr(1:3,3) = inv_skew(C3);

Fr(:,4:6) = [zeros(3);eye(3)];
end