% ===================================
% SCRIPT  camera_define.m
% -----------
% DESCRIPTION:  Define the intrinsic parameters of the camera and save to
%               a mat file.
% OUTPUT:   camera_param.mat    A mat file containing camera calibration matrix.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================

% Intrinsic parameters
% 5.14mm x3.50mm Image sensor, 640 x 480 pixels, f = 3.67mm (Logitech C920)

ku = 640/(5.14e-3); % pixel per unit length in u
kv = 480/(3.50e-3); % pixel per unit length in v
u0 = 640/2;   % principal point in u
v0 = 480/2;   % principal point in v
f = 3.67e-3;  % focul length
K = [ku*f, 0, u0;  % Camera calibration matrix
     0, kv*f, v0;
     0, 0, 1];
R_bc = [0, -1,  0; % Rotation matrix from the body coord. to the camera coord.
        0,  0, -1;
        1,  0,  0]; 
d_bc = 0.1; % Distance between the body frame and the camera frame (>0).
save('camera_param.mat', 'K', 'R_bc', 'd_bc');