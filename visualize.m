function true_x_cum = visualize(true_r, true_x, true_x_cum, timestep, est_r, est_x, z, visible_flag, true_Rot_traj2w, true_trans_traj2w)
    function [true_camera_pose, body_pos, body_att, est_traj_pos, est_traj_att, true_x, est_x, true_x_cum] = convert2world(true_r, true_x, est_r, est_x, true_x_cum, true_Rot_traj2w, true_trans_traj2w, R_bc, d_bc)
        % In the trajectory coordinates.
        true_t = true_r(4:6);
        true_w = true_r(1:3);
        est_t = est_r(4:6);
        est_w = est_r(1:3);
        
        % Body position and rotation in the world coordinates. (Known)
        body_pos = true_Rot_traj2w*true_t + true_trans_traj2w;
        body_att = inv_skew(logm(true_Rot_traj2w*expm(skew(true_w))));
        if norm(body_att) > pi; % Avoid singularities.
            body_att = (1 - 2*pi/norm(body_att))*body_att;
        end
        
        % Estimated trajectory coordinates.
        est_traj_att = inv_skew(logm(expm(skew(body_att))*expm(skew(est_w))'));
        if norm(est_traj_att) > pi; % Avoid singularities.
            est_traj_att = (1 - 2*pi/norm(est_traj_att))*est_traj_att;
        end
        est_traj_pos = body_pos - expm(skew(est_traj_att))*est_t;
        
        % True sender position.
        true_x = true_Rot_traj2w*true_x + true_trans_traj2w;
        true_x_cum(:,end+1) = true_x;
        
        % Estimated sender position.
        est_x = expm(skew(est_traj_att))*est_x + est_traj_pos;
        
        % calculate camera pose in the world coordinates.(external parameters matrix with [R|t])
        true_camera_rot = expm(skew(body_att))*R_bc';
        true_camera_trans = body_pos + expm(skew(body_att))*[d_bc;0;0];
        true_camera_pose = [true_camera_rot, true_camera_trans];
    end

load('camera_param.mat', 'K', 'R_bc', 'd_bc');
u_max = 2*K(1,3);
v_max = 2*K(2,3);

% values are in the world coordinates.
[true_camera_pose, body_pos, body_att, est_traj_pos, est_traj_att, true_x, est_x, true_x_cum] = convert2world(true_r, true_x, est_r, est_x, true_x_cum, true_Rot_traj2w, true_trans_traj2w, R_bc, d_bc);

clf(figure(1));
figure(1);
set(gcf, 'Position', [1050, 400, 0.5*u_max, 0.5*v_max]);
hold on;
set(gca,'Ydir','reverse');
if visible_flag == 1
    scatter(z(1), z(2), 'ro', 'linewidth',2.0);
end
xlabel('u');
ylabel('v');
plot([0,u_max,u_max,0,0],[0,0,v_max,v_max,0],'m');
axis equal;
drawnow;

clf(figure(2));
figure(2);
set(gcf, 'Position', [100, 100, 900, 700]);
grid on; hold on;
% draw world axes.
line([0,4],[0,0],[0,0],'Linestyle','-','Color','r');
line([0,0],[0,4],[0,0],'Linestyle','-','Color','g');
line([0,0],[0,0],[0,4],'Linestyle','-','Color','b');
xlabel('X_w'); ylabel('Y_w'); zlabel('Z_w');
% draw true trajectory axes.
e1_traj = true_Rot_traj2w*[4;0;0] + true_trans_traj2w;
e2_traj = true_Rot_traj2w*[0;4;0] + true_trans_traj2w;
e3_traj = true_Rot_traj2w*[0;0;4] + true_trans_traj2w;
line([true_trans_traj2w(1),e1_traj(1)],[true_trans_traj2w(2),e1_traj(2)],[true_trans_traj2w(3),e1_traj(3)],'Linestyle','-','Color','r');
line([true_trans_traj2w(1),e2_traj(1)],[true_trans_traj2w(2),e2_traj(2)],[true_trans_traj2w(3),e2_traj(3)],'Linestyle','-','Color','g');
line([true_trans_traj2w(1),e3_traj(1)],[true_trans_traj2w(2),e3_traj(2)],[true_trans_traj2w(3),e3_traj(3)],'Linestyle','-','Color','b');
% draw estimated trajectory axes.
e1_traj_est = expm(skew(est_traj_att))*[4;0;0] + est_traj_pos;
e2_traj_est = expm(skew(est_traj_att))*[0;4;0] + est_traj_pos;
e3_traj_est = expm(skew(est_traj_att))*[0;0;4] + est_traj_pos;
line([est_traj_pos(1),e1_traj_est(1)],[est_traj_pos(2),e1_traj_est(2)],[est_traj_pos(3),e1_traj_est(3)],'Linestyle','-.','Color','r');
line([est_traj_pos(1),e2_traj_est(1)],[est_traj_pos(2),e2_traj_est(2)],[est_traj_pos(3),e2_traj_est(3)],'Linestyle','-.','Color','g');
line([est_traj_pos(1),e3_traj_est(1)],[est_traj_pos(2),e3_traj_est(2)],[est_traj_pos(3),e3_traj_est(3)],'Linestyle','-.','Color','b');

%{
% draw camera axes.
e1_cam = true_camera_pose(:,1:3)*[2;0;0] + true_camera_pose(:,4);
e2_cam = true_camera_pose(:,1:3)*[0;2;0] + true_camera_pose(:,4);
e3_cam = true_camera_pose(:,1:3)*[0;0;2] + true_camera_pose(:,4);
line([true_camera_pose(1,4),e1_cam(1)],[true_camera_pose(2,4),e1_cam(2)],[true_camera_pose(3,4),e1_cam(3)],'Linestyle','-','Color','r');
line([true_camera_pose(1,4),e2_cam(1)],[true_camera_pose(2,4),e2_cam(2)],[true_camera_pose(3,4),e2_cam(3)],'Linestyle','-','Color','g');
line([true_camera_pose(1,4),e3_cam(1)],[true_camera_pose(2,4),e3_cam(2)],[true_camera_pose(3,4),e3_cam(3)],'Linestyle','-','Color','b');
axis equal;
%}

% draw body axes.
e1_body = expm(skew(body_att))*[2;0;0] + body_pos;
e2_body = expm(skew(body_att))*[0;2;0] + body_pos;
e3_body = expm(skew(body_att))*[0;0;2] + body_pos;
line([body_pos(1), e1_body(1)],[body_pos(2), e1_body(2)],[body_pos(3),e1_body(3)],'LineStyle','-','Color','r');
line([body_pos(1), e2_body(1)],[body_pos(2), e2_body(2)],[body_pos(3),e2_body(3)],'LineStyle','-','Color','g');
line([body_pos(1), e3_body(1)],[body_pos(2), e3_body(2)],[body_pos(3),e3_body(3)],'LineStyle','-','Color','b');

if timestep ~= 1
    scatter3(true_x_cum(1,1:end-1),true_x_cum(2,1:end-1),true_x_cum(3,1:end-1),'y');
else
    true_x_cum = true_x;
end
scatter3(body_pos(1),body_pos(2),body_pos(3),'r');
scatter3(true_x_cum(1,end),true_x_cum(2,end),true_x_cum(3,end),'r');
scatter3(est_x(1),est_x(2),est_x(3),'b');
plotCamera('Location',true_camera_pose(:,4)','Orientation',transpose(true_camera_pose(:,1:3)),'Opacity',0, 'Size',0.4); 

axis equal;
xlim([-5,25]);
ylim([-5,25]);
zlim([0,10]);
azimuth = 180/pi*(1/2*(atan2(body_pos(1), body_pos(2)) + atan2(true_x(1), true_x(2))) + 90);
alt = 25;
view(azimuth,alt); % change default view angle.

drawnow;
end