function [r,x] = state_trans(r,x,u,Codebook,timestep,true_class)
w = r(1:3);
t = r(4:6);
t = t + expm(skew(w))*[u(1);u(2);u(3)];
w = inv_skew(logm(expm(skew(w))*expm(skew([0;0;u(4)]))));
% Avoid singularities.
if norm(w) > pi;
    w = (1 - 2*pi/norm(w))*w;
end
r = [w;t];
x = x + Codebook{true_class}(:,timestep);
end