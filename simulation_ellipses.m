clear;clc;close all;
numsteps = 20;
max_steps = 10*numsteps+1;
Codebook = code_define_ellipses(numsteps);
true_class = 3;


arr_true_r = zeros(6,max_steps);
arr_true_x = zeros(3,max_steps);
arr_mu = cell(1,max_steps);
arr_Sigma = cell(1,max_steps);
arr_log_phi = zeros(length(Codebook),max_steps);
arr_z = zeros(2,max_steps);
arr_visible_flag = zeros(1,max_steps);
arr_time = 1:1:max_steps;
arr_u = zeros(4,max_steps-1);

% Define trajectory coordinates (in world coordinates).
%w_traj2w = [0.3;0.9;1.3];
%w_traj2w = [rand();rand();rand()];
w_traj2w = [0;0;pi/6];
true_Rot_traj2w = expm(skew(w_traj2w));
true_trans_traj2w = [10;5;5];

% Initial configuration (in trajectory coordinates).
true_r = [inv_skew(logm(true_Rot_traj2w')); true_Rot_traj2w'*[-8;0;-1.5]];
true_x = [0;0;0];

% Randomly initialize belief and use DLT&LM algorithms.
mu = cell(length(Codebook),1);
for ii = 1:length(Codebook)
    w_initguess = [rand();rand();rand()];
    w_initguess = pi*rand()*w_initguess./norm(w_initguess);
    t_initguess = expm(skew(w_initguess))*[-20;0;0];
    mu{ii} = [w_initguess; t_initguess; [0;0;0]];
    %mu{ii} = [true_r; true_x];
    %Sigma{ii} = [1e5*eye(6), zeros(6,3); zeros(3,6), eye(3)];
    %log_phi = [1/3,1/3,1/3];
end
[mu, Sigma, log_phi, z_concat] = initialize_belief(mu, true_r, true_x, numsteps, Codebook, true_class, true_Rot_traj2w, true_trans_traj2w);
[~,likely] = max(log_phi);
while likely == max(log_phi)
    [mu, Sigma, log_phi, z_concat] = initialize_belief(mu, true_r, true_x, numsteps, Codebook, true_class, true_Rot_traj2w, true_trans_traj2w);
    [~,likely] = max(log_phi);
end
fprintf('Step: 1 - 1\n');
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
disp('Class entropy:');
disp(-dot(retrieve(log_phi), log_phi));
disp('Mixand entropies:');
disp(mixand_entropy);
fprintf('\n');
arr_true_r(:,1) = true_r;
arr_true_x(:,1) = true_x;
arr_mu{1} = mu;
arr_Sigma{1} = Sigma;
arr_log_phi(:,1) = log_phi;
arr_z(:,1) = z_concat(end-1:end);
arr_visible_flag(1) = 1; % This is an assumption.


true_x_cum = [];
cycle = 1;
timestep = 1;
for global_timestep = 2:max_steps
    if timestep == numsteps+1;
        timestep = 1;
        cycle = cycle + 1;
    end
    fprintf('Step: %d - %d\n', cycle, timestep);
    u = control_state(log_phi, mu, Sigma, Codebook, timestep);
    %u = control_state(log_phi, mu, Sigma, Codebook, timestep, lambda1, lambda2);
    [true_r, true_x, mu, Sigma, log_phi, z, visible_flag] = ekf_update(true_r, true_x, mu, Sigma, log_phi, u, Codebook, timestep, true_class);
    if visible_flag == 0
        disp('Sender Lost.');
        break;
    end
    disp('Log of phi:');
    disp(log_phi');
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    disp('Class entropy:');
    disp(-dot(retrieve(log_phi), log_phi));
    disp('Mixand entropies:');
    disp(mixand_entropy);
    [~,maxindex] = max(log_phi);
    %maxindex = 1;
    true_x_cum = visualize(true_r, true_x, true_x_cum, timestep, mu{maxindex}(1:6), mu{maxindex}(7:9), z, visible_flag, true_Rot_traj2w, true_trans_traj2w);
    fprintf('\n');
    arr_true_r(:,global_timestep) = true_r;
    arr_true_x(:,global_timestep) = true_x;
    arr_mu{global_timestep} = mu;
    arr_Sigma{global_timestep} = Sigma;
    arr_log_phi(:,global_timestep) = log_phi;
    arr_z(:,global_timestep) = z;
    arr_visible_flag(global_timestep) = visible_flag;
    arr_u(:,global_timestep-1) = u;
    
    timestep = timestep + 1;
end
save('sim_results.mat', 'arr_true_r', 'arr_true_x', 'arr_mu', 'arr_Sigma', 'arr_log_phi', 'arr_z', 'arr_visible_flag', 'arr_u', 'arr_time', 'numsteps', 'true_class', 'true_Rot_traj2w', 'true_trans_traj2w');