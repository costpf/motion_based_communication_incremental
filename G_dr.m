function Gr = G_dr(r,x)
load('camera_param.mat');
P = proj_mat(r);
proj = P*[x;1];
u = proj(1)/proj(3);
v = proj(2)/proj(3);
du_dP = [   x(1),    x(2),    x(3),  1;
               0,       0,       0,  0;
         -x(1)*u, -x(2)*u, -x(3)*u, -u]./proj(3);
dv_dP = [      0,       0,       0,  0;
            x(1),    x(2),    x(3),  1;
         -x(1)*v, -x(2)*v, -x(3)*v, -v]./proj(3);

w = [r(1);r(2);r(3)];
t = [r(4);r(5);r(6)];

[R1,R2,R3] = deriv_expm_rot(w);
dP_dw1 = K*R_bc*[R1',-R1'*t];
dP_dw2 = K*R_bc*[R2',-R2'*t];
dP_dw3 = K*R_bc*[R3',-R3'*t];

dP_dt1 = -K*R_bc*[zeros(3),expm(skew(w))'*[1;0;0]];
dP_dt2 = -K*R_bc*[zeros(3),expm(skew(w))'*[0;1;0]];
dP_dt3 = -K*R_bc*[zeros(3),expm(skew(w))'*[0;0;1]];
     
g11 = sum(sum(du_dP.*dP_dw1));
g12 = sum(sum(du_dP.*dP_dw2));
g13 = sum(sum(du_dP.*dP_dw3));
g14 = sum(sum(du_dP.*dP_dt1));
g15 = sum(sum(du_dP.*dP_dt2));
g16 = sum(sum(du_dP.*dP_dt3));

g21 = sum(sum(dv_dP.*dP_dw1));
g22 = sum(sum(dv_dP.*dP_dw2));
g23 = sum(sum(dv_dP.*dP_dw3));
g24 = sum(sum(dv_dP.*dP_dt1));
g25 = sum(sum(dv_dP.*dP_dt2));
g26 = sum(sum(dv_dP.*dP_dt3));

Gr = [g11, g12, g13, g14, g15, g16;
      g21, g22, g23, g24, g25, g26];
end