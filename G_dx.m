function Gx = G_dx(r,x)
P = proj_mat(r);
proj = P*[x;1];
u = proj(1)/proj(3);
v = proj(2)/proj(3);
Gx = [P(1,1) - P(3,1)*u, P(1,2) - P(3,2)*u, P(1,3) - P(3,3)*u;
      P(2,1) - P(3,1)*v, P(2,2) - P(3,2)*v, P(2,3) - P(3,3)*v]./proj(3);
end