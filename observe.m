function z_no_noise = observe(r,x)
P = proj_mat(r);
proj = P*[x;1];
u = proj(1)/proj(3);
v = proj(2)/proj(3);
z_no_noise = [u;v];
end