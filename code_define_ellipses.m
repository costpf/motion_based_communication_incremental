function Codebook = code_define_ellipses(numsteps)
T = linspace(0, 2*pi, numsteps);
dT = T(2) - T(1);
Codebook = cell(4,1);
for i = 1:4;
    Codebook{i} = zeros(3, length(T));
end
for i = 1:length(T);
    Codebook{1}(:,i) = [-cos(T(i)); 0; -sin(T(i))]*dT; %Codebook1: Circular Trajectory
    Codebook{2}(:,i) = [-0.75*cos(T(i)); 0; -sin(T(i))]*dT; % Codebook2: Elliptic Trajectory
    Codebook{3}(:,i) = [-0.5*cos(T(i)); 0; -sin(T(i))]*dT; % Codebook3: Elliptic Trajectory 2
    Codebook{4}(:,i) = [-0.25*cos(T(i)); 0; -sin(T(i))]*dT; % Codebook4: Elliptic Trajectory 3
end
end