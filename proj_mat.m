function P = proj_mat(r)
load('camera_param.mat');
w = [r(1);r(2);r(3)];
t = [r(4);r(5);r(6)];
R = expm(skew(w))';
t = -R*t - [d_bc;0;0];
P = K*R_bc*[R,t]; % 4x3 Projection Matrix.
end