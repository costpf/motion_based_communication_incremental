function playback_sim_ellipses(filename)
load(filename);
Codebook = code_define_ellipses(numsteps);
true_x_cum = [];

true_r = arr_true_r(:,1);
true_x = arr_true_x(:,1);
visible_flag = arr_visible_flag(1);
z = arr_z(:,1);
log_phi = arr_log_phi(:,1);
Sigma = arr_Sigma{1};
mu = arr_mu{1};
fprintf('Step: 1 - 1\n');
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
disp('Class entropy:');
disp(-dot(retrieve(log_phi), log_phi));
disp('Mixand entropies:');
disp(mixand_entropy);
fprintf('\n');
[~,maxindex] = max(log_phi);
visualize(true_r, true_x, true_x_cum, 1, mu{maxindex}(1:6), mu{maxindex}(7:9), z, visible_flag, true_Rot_traj2w, true_trans_traj2w)

cycle = 1;
timestep = 1;
for tt = 2:length(arr_time)
    if timestep == numsteps + 1
        timestep = 1;
        cycle = cycle + 1;
    end
    
    true_r = arr_true_r(:,tt);
    true_x = arr_true_x(:,tt);
    visible_flag = arr_visible_flag(tt);
    z = arr_z(:,tt);
    log_phi = arr_log_phi(:,tt);
    Sigma = arr_Sigma{tt};
    mu = arr_mu{tt};
    
    fprintf('Step: %d - %d\n', cycle, timestep);
    u = arr_u(:,tt-1);
    disp('Control Input:');
    disp([u(1:3)', u(4)*180/pi]);
    
    if visible_flag == 0
        disp('Sender Lost.');
        break;
    end
    disp('Log of phi:');
    disp(log_phi');
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    disp('Class entropy:');
    disp(-dot(retrieve(log_phi), log_phi));
    disp('Mixand entropies:');
    disp(mixand_entropy);
    [~,maxindex] = max(log_phi);
    true_x_cum = visualize(true_r, true_x, true_x_cum, timestep, mu{maxindex}(1:6), mu{maxindex}(7:9), z, visible_flag, true_Rot_traj2w, true_trans_traj2w);
    timestep = timestep + 1;
end
end