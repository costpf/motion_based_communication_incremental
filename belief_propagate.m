function [r_bar, x_bar, Sigma_bar, F, G, H, K, Sigma] = belief_propagate(r,x,Sigma,u,Codebook,timestep,true_class)
% Noise Parameters.
load('noise_param.mat');
Fr = F_dr(r, u); % Jacobian for r evaluated at the previous state.
% State transition.
[r_bar, x_bar] = state_trans(r,x,u,Codebook,timestep,true_class);
F = [Fr,       zeros(6,3);
     zeros(3,6), eye(3)];
Q = [Qr, zeros(6,3);
     zeros(3,6), Qx];

Sigma_bar = F*Sigma*F' + Q;
Sigma_bar = (Sigma_bar + Sigma_bar')/2; %to make sure it's symmetric.

G = [G_dr(r_bar, x_bar), G_dx(r_bar, x_bar)];
H = G*Sigma_bar*G' + R;
H = (H + H')/2; %to make sure it's symmetric.
K = Sigma_bar*G'/H;

Sigma = (eye(9) - K*G)*Sigma_bar;
Sigma = (Sigma + Sigma')/2; %to make sure it's symmetric
end