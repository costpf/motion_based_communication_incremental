function logdet = logdet(A)
% Calculate log(det(A)) accurately using Cholesky decomposition for
% positive-definite, symmetric matrix A.
[L,not_positive] = chol(A);
if not_positive ~= 0
    %disp('The matrix is not positive definite.');
    logdet = log(det(A));
else
    logdet = 2*sum(log(diag(L)));
end