function [true_r, true_x, mu, Sigma, log_phi, z, visible_flag] = ekf_update(true_r, true_x, mu, Sigma, log_phi, u, Codebook, timestep, true_class)
load('noise_param.mat');

% True State Transition
[true_r, true_x] = state_trans(true_r, true_x, u, Codebook, timestep, true_class);
true_r = true_r + transpose(mvnrnd([0;0;0;0;0;0],Qr));
true_x = true_x + transpose(mvnrnd([0;0;0],Qx));

% Take a new noisy observation.
[z,visible_flag] = observe_actual(true_r, true_x);

% MHEKF update. Treat y = [r' x'] in R^9.
for ii = 1:length(log_phi)
    % State transition estimation
    w = mu{ii}(1:3); % previous state.
    t = mu{ii}(4:6); % previous state.
    x = mu{ii}(7:9); % previous state.
    r = [w; t];
    [r_bar,x_bar,~,~,~,H,K,Sigma{ii}] = belief_propagate(r,x,Sigma{ii},u,Codebook,timestep,ii);
    mu_bar = [r_bar; x_bar];
    z_est = observe(r_bar, x_bar);
    innov = z - z_est;
    mu{ii} = mu_bar + K*innov;
    % Avoid singularities
    if norm(mu{ii}(1:3)) > pi;
        mu{ii}(1:3) = (1 - 2*pi/norm(mu{ii}(1:3)))*mu{ii}(1:3);
    end
    log_phi(ii) = -1/2*logdet(2*pi*H) - 1/2*innov'*(H\innov) + log_phi(ii);
end
log_phi = log_phi - repmat(max(log_phi),length(Codebook),1);

% Can we do this?
for ii = 1:length(log_phi)
    if log_phi(ii) < -7.45e2
        log_phi(ii) = -7.45e2; %exp(-7.46e2) == 0, exp(-7.45e2) = 4.9407e-324
    end
end

end