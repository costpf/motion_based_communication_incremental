function z = observe_concat(r,x_concat)
numpoints = length(x_concat)/3;
P = proj_mat(r);
x_concat = reshape(x_concat,3,numpoints);
z = zeros(2,numpoints);
for ii = 1:numpoints
    proj = P*[x_concat(:,ii);1];
    u = proj(1)/proj(3);
    v = proj(2)/proj(3);
    z(:,ii) = [u;v];
end
z = reshape(z,2*numpoints,1);
end