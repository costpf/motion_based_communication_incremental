function s = logsumexp(log_coeffs,xs)
% Compute log(sum_i^n exp(log(lambda_i)*x_i)).
y = log_coeffs+xs;
y_max = max(y);
s = y_max + log(sum(exp(y - y_max)));
if isinf(s)
    disp('Overflow or Underflow occured!')
end
end