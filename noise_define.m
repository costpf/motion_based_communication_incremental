% Noise Covariance Parameters
Qw_new = 0.00001*eye(3); % Receiver's rotational noise (3-by-3)
Qt_new = 0.00005*eye(3); % Receiver's translational noise (3-by-3)
Qx_new = 0.0001*eye(3); % Sender's translational noise (3-by-3)
R_new = 3*eye(2); % Observation noise (2-by-2)

Qr_new = [Qw_new,  zeros(3);
         zeros(3), Qt_new];
filename = 'noise_param.mat';
if exist(filename, 'file')
    load(filename);
    if ~(isequal(Qr, Qr_new) && isequal(Qx, Qx_new) && isequal(R, R_new))
        Qr = Qr_new;
        Qx = Qx_new;
        R = R_new;
        save(filename, 'Qr','Qx','R');
    end
else
    Qr = Qr_new;
    Qx = Qx_new;
    R = R_new;
    save(filename, 'Qr','Qx','R');
end