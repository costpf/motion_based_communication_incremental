function u = control_state_2(log_phi, mu, Sigma, Codebook, timestep)
tic;
trans_max = 1.0;
rot_max = 10*pi/180;
numdisc = 9;
space_yaw = linspace(-rot_max,rot_max,numdisc);
space_y = linspace(-trans_max,trans_max,numdisc);

inputs = zeros(4,numdisc);
costs = zeros(1,numdisc);

for ii = 1:numdisc;
    cost_for_y = zeros(1,numdisc);
    dx = 0;
    dz = 0;
    for jj = 1:numdisc
        dy = space_y(jj);
        inputs_temp = [dx;dy;dz;space_yaw(ii)];
        [~, cost_for_y(jj)] = feval(@J_state, log_phi, mu, Sigma, inputs_temp, Codebook, timestep);
    end
    [min_cost_for_y,min_index_for_y] = min(cost_for_y);
    if min_cost_for_y > 200^2;
        costs(ii) = Inf;
    else
        inputs(:,ii) = [dx, space_y(min_index_for_y), dz, space_yaw(ii)];
        [costs(ii),] = feval(@J_state, log_phi, mu, Sigma, inputs(:,ii), Codebook, timestep);
    end
end
disp(costs);
[~,minindex] = min(costs);
u = inputs(:, minindex);
disp('Control Input:');
disp([u(1:3)', u(4)*180/pi])
%disp('Costs:');
%fprintf('J1 = %.2f, J2 = %.2f, J3 = %.2f\n', costs(1,minindex), lambda1*costs(2,minindex), lambda2*costs(3,minindex));
toc;
end