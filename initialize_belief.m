function [mu, Sigma, log_phi, z] = initialize_belief(mu, true_r, true_x, numsteps, Codebook, true_class, true_Rot_traj2w, true_trans_traj2w)
true_x_cum = [];
z = zeros(2*numsteps+2,1);
z(1:2) = observe_actual(true_r, true_x);
for prep_time = 1:numsteps
    [true_r, true_x] = state_trans(true_r,true_x,zeros(4,1),Codebook,prep_time,true_class);
    [z_partial,visible_flag] = observe_actual(true_r, true_x);
    true_x_cum = visualize(true_r, true_x, true_x_cum, prep_time, mu{1}(1:6), mu{1}(7:9), z_partial, visible_flag, true_Rot_traj2w, true_trans_traj2w);
    z(2*prep_time+1:2*prep_time+2) = z_partial;
end
Codebook_cum = cell(length(Codebook),1);
for ii = 1:length(Codebook)
    Codebook_cum{ii} = zeros(numsteps+1,3);
    for jj = 1:numsteps
        Codebook_cum{ii}(jj+1,:) = Codebook_cum{ii}(jj,:) + Codebook{ii}(:,jj)';
    end
    Codebook_cum{ii} = reshape(transpose(Codebook_cum{ii}),3*(numsteps+1),1);
end
[mu, Sigma, log_phi] = prior_rob(z, Codebook_cum);
end