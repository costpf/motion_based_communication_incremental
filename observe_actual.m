function [z,visible_flag] = observe_actual(r,x)
load('camera_param.mat'); load('noise_param.mat');
U0 = K(1,3);
V0 = K(2,3);

P = proj_mat(r);
proj = P*[x;1];
u = proj(1)/proj(3);
v = proj(2)/proj(3);
z = [u;v] + transpose(mvnrnd([0;0],R));

if proj < 0;
    visible_flag = 0;
elseif ((u < 0 || u > 2*U0) || (v < 0 || v > 2*V0))
    visible_flag = 0;
else
    visible_flag = 1;
end

end